<?php
/**
 * Created by PhpStorm.
 * User: Anton
 * Date: 28.03.2020
 * Time: 23:26
 */

namespace DataSuggestion;

use Illuminate\Support\Facades\Facade;

class DataSuggestionFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return DataSuggestionClient::class;
    }
}