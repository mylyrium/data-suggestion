<?php
/**
 * Created by PhpStorm.
 * User: Anton
 * Date: 28.03.2020
 * Time: 21:54
 */

namespace DataSuggestion;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class AppServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/data-suggestion.php' => config_path('data-suggestion.php'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/data-suggestion.php', 'data-suggestion'
        );

        $this->app->bind(DataSuggestionClient::class);
    }
}