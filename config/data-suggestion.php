<?php

return [
    'api_key'         => '',
    'proxy'           => null,
    'base_url'        => '',
    'timeout'         => 20,
    'tries'           => 2,
    'seconds'         => 10,
    'debug'           => false,
    'track_redirects' => false,
];